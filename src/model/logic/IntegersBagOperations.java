package model.logic;

import java.util.Iterator;

import model.data_structures.IntegersBag;

public class IntegersBagOperations {

	
	
	public double computeMean(IntegersBag bag){
		double mean = 0;
		int length = 0;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				mean += iter.next();
				length++;
			}
			if( length > 0) mean = mean / length;
		}
		return mean;
	}
	
	
	public int getMax(IntegersBag bag){
		int max = Integer.MIN_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( max < value){
					max = value;
				}
			}
			
		}
		return max;
	}
	
	public int getMin(IntegersBag bag){
		int min = Integer.MAX_VALUE;
	    int value;
		if(bag != null){
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext()){
				value = iter.next();
				if( min > value){
					min = value;
				}
			}
			
		}
		if(min== Integer.MAX_VALUE)
			return min;
		else 
			return 0;
	}
	
	public int getModa(IntegersBag bag)
	{
		int cont = 0;
		int value;
		int actual;
		int respuesta=0;
		if(bag!=null)
		{
			Iterator<Integer> iter = bag.getIterator();
			while(iter.hasNext())
			{
				int cont1= 0;
				actual = iter.next();
				Iterator<Integer> iter1 = bag.getIterator();
				while(iter.hasNext())
				{
					value = iter1.next();
					if(value==actual)
						cont1++;
				}
				if(cont1>cont)
				{
					cont = cont1;
					actual = respuesta;
				}
			}
		}
		return respuesta;
	}
	
	public String darRango(IntegersBag bag)
	{
		return "El rango es: "+getMin(bag)+", "+getMax(bag);
	}
	
}
